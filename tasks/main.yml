---
# tasks file for mergetb_portal_helm
# get platform specific variables
- name: Gather os specific variables
  ansible.builtin.include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution | lower }}-{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution | lower }}.yml"
    - "{{ ansible_os_family | lower }}-{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_os_family | lower }}.yml"
  tags: vars

- name: Main.yml packages related to helm
  ansible.builtin.package:
    name: "{{ helm_packages }}"
    state: present
  vars:
    helm_packages:
      - helm
      - python3-kubernetes
      - python3-pyyaml
  when: ('k8smasters' in group_names)

- name: Main.yml check for helm diff plugin
  ansible.builtin.shell:
    cmd: set -o pipefail && helm plugin list | grep ^diff
  when: ('k8smasters' in group_names)
  register: helm_diffplugin_check
  changed_when: helm_diffplugin_check.rc != 0 # only install plugin when plugin check fails

- name: Main.yml install helm diff plugin for better ansible output
  ansible.builtin.shell:
    cmd: set -o pipefail && helm plugin install https://github.com/databus23/helm-diff
  when: ('k8smasters' in group_names) and (helm_diffplugin_check is changed)
  register: helm_diffplugin_installed
  changed_when: helm_diffplugin_installed is successful

# this may require some additional plumbing (list/store version, update, check/list version to relay a changed state)
# - name: Main.yml update helm diff plugin (always)
#   ansible.builtin.shell:
#   # note: this will always download and update, even if updating to "same" version :(
#     cmd: set -o pipefail && helm plugin update diff
#   when: ('k8smasters' in group_names)

- name: Main.yml helm autocompletion on hosts
  ansible.builtin.shell:
    cmd: set -o pipefail && helm completion bash | tee /etc/bash_completion.d/helm
    creates: /etc/bash_completion.d/helm
  when: ('k8smasters' in group_names)

# go ahead and add the mergetb repo so we can trigger the
# helm repo updates even when we are doing this the first time.
- name: Main.yml install mergetb helm repo
  kubernetes.core.helm_repository:
    state: present
    repo_name: mergetb
    repo_url: https://gitlab.com/api/v4/projects/36949361/packages/helm/stable
  when: ('k8smasters' in group_names)
# ensure non-existent repo absent to force refresh
- name: Main.yml Update Helm repos
  kubernetes.core.helm:
    name: dummy
    namespace: kube-system
    state: absent
    update_repo_cache: true
  when: ('k8smasters' in group_names)
  tags:
    - helm
    - loki
    - portal
    - launch
    - observability
    - prometheus
    - promtail
    - stepca
    - cert-manager

# do cert-manager first because it installs globally accessible resources
# and it gives us a "default" helm repo in order to "update_repo_cache"
- name: Main.yml include cert-manager configs
  ansible.builtin.import_tasks: cert_manager.yml
  when: ('k8smasters' in group_names and k8s_use_certmanager is true)
  tags:
    - cert-manager

- name: Main.yml include ingress-nginx ingress controller
  ansible.builtin.import_tasks: dependency_ingress.yml
  when: ('k8smasters' in group_names)
  tags:
    - nginx
    - ingress

# fedora/rhel specific
- name: Main.yml shared config for ceph-csi
  when: (k8s_use_cephcsi_plugin is true and ansible_os_family == "RedHat")
  tags:
    - cephfs
    - cephcsi
    - ceph-csi
  block:
    - name: Main.yml include cephfs mounter
      ansible.builtin.package:
        name:
          - ceph-common
          - container-selinux
        state: present
    - name: Main.yml ensure container_use_cephfs sebool is set (live)
      ansible.posix.seboolean:
        name: container_use_cephfs
        state: true
      ignore_errors: "{{ ansible_check_mode }}"

    - name: Main.yml ensure container_use_cephfs sebool is set (persistent)
      ansible.posix.seboolean:
        name: container_use_cephfs
        persistent: true
        state: true
      ignore_errors: "{{ ansible_check_mode }}"


- name: Main.yml include cephfs plugins
  ansible.builtin.import_tasks: storage_cephcsi.yml
  when: ('k8smasters' in group_names and k8s_use_cephcsi_plugin is true)
  tags:
    - cephfs
    - cephcsi
    - ceph-csi
    - pv


- name: Main.yml include nfs storage
  ansible.builtin.import_tasks: nfs_volumes.yml
  when: ('k8smasters' in group_names and k8s_use_nfs_volumes is true)
  tags:
    - nfs
    - nfs_pv
    - pv

- name: Main.yml include postgres/kratos (merge auth)
  ansible.builtin.import_tasks: dependency_auth.yml
  when: ('k8smasters' in group_names)
  tags:
    - postgres
    - kratos
    - auth
    - merge-auth

- name: Main.yml include smallstep/step-ca
  ansible.builtin.import_tasks: dependency_stepca.yml
  when: ('k8smasters' in group_names)
  tags:
    - stepca
    - step
    - smallstep

# currently these all require cephfs-csi, but it can be altered,
# or, alternatively, a different task file can exist for different
# methods of storage. might want to refactor dependency storage as well
# if that is the case
- name: Main.yml include portal storage PV/PVC creations
  ansible.builtin.import_tasks: mergetb_portal_storage.yml
  when: ('k8smasters' in group_names)
  tags:
    - portal
    - storage

- name: Main.yml include mergetb_portal helm install
  ansible.builtin.import_tasks: mergetb_portal.yml
  when: ('k8smasters' in group_names)
  tags:
    - portal

- name: Main.yml include mergetb_portal_launch helm install
  ansible.builtin.import_tasks: mergetb_launch.yml
  when: ('k8smasters' in group_names)
  tags:
    - launch

- name: Main.yml include backup_config.yml push backups k8s CronJob
  ansible.builtin.import_tasks: backup_config.yml
  when: ('k8smasters' in group_names and mergetb_portal_backups is true)
  tags:
    - backups

- name: Main.yml do an email relay with helm
  ansible.builtin.import_tasks: helm_email_relay_container.yml
  when: ('k8smasters' in group_names and mergetb_portal_backups is true)
  tags:
    - email
    - smtp
    - postfix
    - mail

- name: Main.yml install observability applications (loki, grafana)
  ansible.builtin.import_tasks: observability.yml
  when: ('k8smasters' in group_names)
  # tags:
  #   - loki
  #   - grafana
  #   - observability
